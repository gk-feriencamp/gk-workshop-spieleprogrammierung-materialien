function love.load()
	-- import all graphics
	require("graphics")

    -- entity management and movement, (player, enemies and items)
    require("entities")

    -- handles player movement
    require("controls")

    -- handles falling object
    require("fallingObjects")

    -- load global variables
    require("globals")

    -- make background blue (like sky)
    love.graphics.setBackgroundColor(0.9, 0.97, 0.87, 1)

    -- current window size
    _G.windowX, _G.windowY = love.window.getMode()
    _G.time=0

    -- create player entity
    _G.player = f_createEntity()
    _G.player:setTexture(f_createTexture("player", {4, 4, 1, 4, 1}, _G.animationDelay))
    _G.playerYPosition=_G.windowY-_G.zoom*_G.player:getImage():getHeight() - 2
    _G.playerXPosition=_G.windowX-_G.zoom*_G.player:getImage():getWidth()
    _G.player:setPosition(0, _G.playerYPosition)

    -- create random seed
    math.randomseed( os.time() )

    -- create falling entity
    _G.falling = f_createEntity()
    _G.falling:setTexture(f_createTexture("falling", {1, 1, 1, 1}, _G.animationDelay))
    _G.falling:setPosition(0, 0)
    _G.fallingYPosition=_G.windowY
    _G.fallingXPosition=_G.windowX
    _G.fallingObjects={}

    -- indicates if a key is pressed currently
    _G.keyTable = {}

    -- player acceleration
    _G.playerAcceleration = 0

    -- used to call function every second
    _G.slowUpdate = 0
end
 
function love.update(dt)
    _G.time = _G.time + dt
    _G.player:update(dt)

    -- handles the player controls
    f_playerUpdate(dt)

    -- handles the falling objects
    f_fallingUpdate(dt)

    -- handles the collision
    f_collision()

    -- functions that will be called every second
    _G.slowUpdate = _G.slowUpdate + dt
    if _G.slowUpdate > 1 then
        collectgarbage()

        -- reset update counter
        _G.slowUpdate = 0
    end
end
 
function love.draw()
    _G.player:draw(_G.zoom,true)
    f_drawFallingObjects()
    love.graphics.print({{0,0,0,100},'Time: '.. _G.time .. "s"},0, 0)
end

-- changed the windows dimension and recreates canvases
function love.resize(_newWidth, _newHeight)
    _G.windowX = _newWidth
    _G.windowY = _newHeight
end

function love.keypressed(_key)
    _G.keyTable[_key] = true
end

function love.keyreleased(_key)
    _G.keyTable[_key] = false
end
