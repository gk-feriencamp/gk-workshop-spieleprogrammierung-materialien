-- speed of animations in seconds
_G.animationDelay = 0.15

-- max player speed
_G.playerMaxAcceleration = 400
_G.playerAccelerationSpeed = 3.8

-- zoom
_G.zoom = 1
_G.factor2Player = 1/3

-- max falling objects
_G.maxObjects = 5

-- meter to pixel
_G.meter2Pixel = 3779.5275590551

-- acceleration for for falling objects
_G.acceleration = 0.0981
_G.angularSpeed = 150
