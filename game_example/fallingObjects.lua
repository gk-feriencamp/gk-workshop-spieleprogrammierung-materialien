function f_fallingUpdate(dt)
  if ((math.random() > 0.9) and (#_G.fallingObjects < _G.maxObjects)) then
    local _a = {}
    _a["mode"] = math.random(1,4)
    _a["time"] = _G.time
    _a["degree0"] = math.random(0,360)
    _a["degree"] = 0
    _a["width"] = 0.5*_G.falling:getImage():getWidth()
    _a["height"] = 0.5*_G.falling:getImage():getHeight()
    _radius = _G.factor2Player/_G.zoom*math.sqrt(_a["width"]*_a["width"]+_a["height"]*_a["height"])
    _a["radius"] = _radius 
    _a["x"] = math.random(_radius,_G.fallingXPosition-_radius)
    _a["y0"] = _radius
    _a["y"] = _radius
    
    table.insert(_G.fallingObjects,_a)
  end
  for key,value in ipairs(_G.fallingObjects) do
    local _time = _G.time-value["time"]
    local _Y    = _G.acceleration/2.0*_time*_time*_G.meter2Pixel + value["y0"]
    local _degr = _G.angularSpeed*_time + value["degree0"]
    if (_Y > _G.fallingYPosition-value["radius"]) then
      table.remove(_G.fallingObjects, key)
    else
      value["y"] = _Y
      value["degree"] = _degr
      --print("X:" .. value["x"] .. ", Y: " .. _Y .. ", radius: " .. value["radius"] )
    end
  end
end
function f_drawFallingObjects()
    for key,value in ipairs(_G.fallingObjects) do
      _G.falling:setTextureMode(value["mode"])
      _G.falling:setPosition(value["x"], value["y"])
      _G.falling:draw(_G.factor2Player/_G.zoom,_,math.pi/180*value["degree"],value["width"],value["height"])
    end
end
function f_collision()
  _Xplayer=_G.player:getX()
  _Yplayer=_G.player:getY()
  _XEndplayer=_Xplayer+_G.zoom*_G.player:getImage():getWidth()
  for key,value in ipairs(_G.fallingObjects) do
    _radius = value["radius"]
    _x = value["x"]
    _y = value["y"]
    if ((_Yplayer < _y + _radius) and (_x+_radius>_Xplayer) and (_x-_radius<_XEndplayer)) then
      --print("Collision")
      _mode = value["mode"]
      if (_mode<2.5) then
        _G.player:addScore(-100)
      else
        _G.player:addScore(100)
      end
      table.remove(_G.fallingObjects, key)
    end
  end
end
