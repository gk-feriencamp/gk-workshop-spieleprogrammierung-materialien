function f_playerUpdate(_updateTime)
    local _movementFactor = _updateTime * _G.playerMaxAcceleration * _G.playerAccelerationSpeed
    local _moved = false
    local _playerX = _G.player:getX()

    -- increase speed
    if _G.keyTable["a"] and _playerX > 0 then
        if _G.playerAcceleration > -_G.playerMaxAcceleration then
            _G.playerAcceleration = _G.playerAcceleration - _movementFactor
        end

        _moved = true

        _G.player:setTextureMode(2)
    end

    if _G.keyTable["d"] and _playerX < _G.playerXPosition then
        if _G.playerAcceleration < _G.playerMaxAcceleration then
            _G.playerAcceleration = _G.playerAcceleration + _movementFactor
        end

        _moved = true

        _G.player:setTextureMode(4)
    end

    -- decrease speed
    if _moved == false and _G.playerAcceleration ~= 0 then
        if _G.playerAcceleration < _movementFactor and _G.playerAcceleration > -_movementFactor then
            _G.playerAcceleration = 0
            _G.player:setTextureMode(1)
        elseif _G.playerAcceleration > 0 then
            _G.playerAcceleration = _G.playerAcceleration - _movementFactor

            _G.player:setTextureMode(5)
        else
            _G.playerAcceleration = _G.playerAcceleration + _movementFactor

            _G.player:setTextureMode(3)
        end
    end

    _G.player:setPositionRelative(_updateTime * _G.playerAcceleration, 0)

    -- check for border and stop
    if _playerX < 0 then
        _G.playerAcceleration = 0
        _G.player:setPosition(0, _G.playerYPosition)
        _G.player:setTextureMode(1)
    end

    if _playerX > _G.playerXPosition then
        _G.playerAcceleration = 0
        _G.player:setPosition(_G.playerXPosition, _G.playerYPosition)
        _G.player:setTextureMode(1)
    end
end
