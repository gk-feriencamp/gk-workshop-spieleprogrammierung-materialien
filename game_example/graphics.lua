-- [[
--  Graphics Object - Inhabits functions for entity sprites and other stuff
--
--  :setMode(mode)
--      Changes the mode of the animation
--
--  :getImage()
--      Returnes the image of the texture
--
--  :getMode()
--      Returnes the mode of the texture
--
--  :animationUpdate()
--      Needs to be triggered in update function, changes the textures to create an animation
--
-- ]]
function f_createTexture(_name, _modeDefinition, _updateTime)
	local _textureObject = {}
	_textureObject._currentMode = 1
	_textureObject._currentPicture = 1
	_textureObject._currentTimestep = 0
	_textureObject._updateTime = _updateTime
	
	-- load the graphics
	for _currentMode = 1, #_modeDefinition do
		_textureObject[_currentMode] = {}
	
		for _currentPicture = 1, _modeDefinition[_currentMode] do
			_textureObject[_currentMode]._pictures = _modeDefinition[_currentMode]
			_textureObject[_currentMode][_currentPicture] = love.graphics.newImage("gfx/" .. _name .. "_" .. _currentMode .. "_" .. _currentPicture .. ".png")
			_textureObject[_currentMode][_currentPicture]:setFilter("nearest", "nearest")
		end
	end
	
	-- change the mode
	function _textureObject:setMode(_newMode)
		self._currentMode = _newMode
		self._currentPicture = 1
	end
	
	-- change the animation
	function _textureObject:animationUpdate(_addedTime)
		if self._currentTimestep > self._updateTime then
			self._currentTimestep = 0
			self._currentPicture = self._currentPicture % self[self._currentMode]._pictures + 1
		else
			self._currentTimestep = self._currentTimestep + _addedTime
		end
	end
	
	-- returns the correct image
	function _textureObject:getImage()
		return self[self._currentMode][self._currentPicture]
	end

	-- returns the current texture mode
	function _textureObject:getMode()
		return self._currentMode
	end
	
	return _textureObject
end
