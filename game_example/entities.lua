--[[
--  Entity Object - Object for all moveable, colidable objects
--
--  :update(dt)
--      apply gravity, changes the animation
--
--  :draw()
--      draws the object with the offset
--
--  :resetPostion()
--      mostly used interally, recalculates the position
--
--  :setOffset(offsetX, offsetY)
--      changes the offset of the texture
--
--  :setPosition(x, y)
--      staticly changes the entity position
--
--  :setPositionRelative(deltaX, deltaY)
--      changes the postion relative to the current position
--
--  :setTexture(texture)
--      sets the new texture
--
--  :setTextureMode(mode)
--      set the new texture mode
--
--  :getX()
--      Guess what ... it returns the X value
--
--  :getY()
--      Guess what ... it returns the Y value
--
--  :getImage()
--      Guess what ... it returns the currentImage
--
--  :addScore()
--      Guess what ... it add given score to the score
--
--]]
function f_createEntity()
    local _entityObject = {}
    
    _entityObject._posX = 0
    _entityObject._posY = 0

    _entityObject._drawOffsetX = 0
    _entityObject._drawOffsetY = 0

    _entityObject._drawX = 0
    _entityObject._drawY = 0

    _entityObject._texture = f_createTexture("nothing", {1}, 99999)
    _entityObject._score = 0
    _entityObject._scorePerSeconds = 0.1

    function _entityObject:update(_updateTime)
        self._score = self._score + _updateTime*self._scorePerSeconds
        self._texture:animationUpdate(_updateTime)
    end

    function _entityObject:draw(_zoom, _anotate, _rotate, _offsetX, _offsetY)
        _anotate = _anotate or false 
        _rotate = _rotate or 0 
        _offsetX = _offsetX or 0 
        _offsetY = _offsetY or 0 
        love.graphics.draw(self._texture:getImage(), self._drawX, self._drawY, _rotate, _zoom, _zoom, _offsetX, _offsetY)
        if (_anotate == true) then
          --love.graphics.print({{0,0,0,100},'Ent_Pos['.. self._drawX .. ", " .. self._drawY .. "]"},self._drawX, self._drawY-10)
          love.graphics.print({{0,0,0,100},'Score: '.. self._score },self._drawX, self._drawY-10)
        end
    end

    function _entityObject:resetPostion()
        self._drawX = self._posX + self._drawOffsetX
        self._drawY = self._posY + self._drawOffsetY
    end

    function _entityObject:setOffset(_newOffsetX, _newOffsetY)
        self._drawOffsetX = _newOffsetX
        self._drawOffsetY = _newOffsetY

        self:resetPostion()
    end

    function _entityObject:setPosition(_newX, _newY)
        self._posX = _newX
        self._posY = _newY

        self:resetPostion()
    end

    function _entityObject:setPositionRelative(_deltaX, _deltaY)
        self._posX = self._posX + _deltaX
        self._posY = self._posY + _deltaY

        self:resetPostion()
    end

    function _entityObject:setTexture(_newTexture)
        self._texture = _newTexture
    end

    function _entityObject:setTextureMode(_newTextureMode)
        if self._texture:getMode() ~= _newTextureMode then
            self._texture:setMode(_newTextureMode)
        end
    end

    function _entityObject:getX()
        return self._posX
    end

    function _entityObject:getY()
        return self._posY
    end

    function _entityObject:getImage()
        return self._texture:getImage()
    end

    function _entityObject:addScore(_score)
        self._score = self._score + _score
    end

    return _entityObject
end
