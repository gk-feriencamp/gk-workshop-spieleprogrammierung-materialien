function love.load()
	showHello = false
	movingLeft = false
	movingRight = false
	
	bild = love.graphics.newImage("bild.png")
	
	x = 0
end

function love.update(_updateTime)
	if movingRight == true then
		x = x + 10
	end
	
	if movingLeft == true then
		x = x - 10
	end
end

function love.draw()
	if showHello == true then
		love.graphics.draw(bild, x, 0)
	end
end

function love.keypressed(_key)
	if _key == "space" then
		showHello = not showHello
	end
	
	if _key == "right" then
		movingRight = true
	end
	
	if _key == "left" then
		movingLeft = true
	end
end

function love.keyreleased(_key)
	if _key == "right" then
		movingRight = false
	end
	
	if _key == "left" then
		movingLeft = false
	end
end







